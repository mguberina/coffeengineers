import os, app


class Config(object):
    SQLALCHEMY_DATABASE_URI='postgresql://postgres:disiburazkemaje@oppdatabase.cozpe3injcaz.eu-central-1.rds.amazonaws.com:5432/opp_database'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_USERNAME = 'postgres'
    SQLALCHEMY_PASSWORD = 'disiburazkemaje'
    SQLALCHEMY_DATABASE_NAME = 'opp_database'

    FLASK_ENV = os.environ.get('FLASK_ENV') 
    FLASK_DEBUG = os.environ.get('FLASK_DEBUG')
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'superultr4t4jnikeyyyy1yb00rassz'


    IMAGE_UPLOADS = "/home/nkosl/coffeengineers/praticijene/app/static/img"
