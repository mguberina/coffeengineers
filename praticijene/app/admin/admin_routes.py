from flask import Blueprint, render_template, request, url_for, redirect
from flask import current_app as app
from app.models import User, iteminstore, Item, Store, Tag, ItemTag, UserTag, commentstore
from flask_login import login_required, current_user
import re
from app import db

admin_bp = Blueprint('admin_bp', __name__,
                    template_folder='templates')

@admin_bp.route('/users')
def users():
    table_users = User.query.filter(User.userid != current_user.userid).order_by(User.userid).all()
    return render_template('users.html', title='Korisnici', users=table_users)


@admin_bp.route('/stores')
def stores():
    table_store = Store.query.order_by(Store.storeid).all()
    return render_template('stores.html', title='Trgovine', stores=table_store)


@admin_bp.route('/changeRole', methods=['POST'])
def change_role():
    userid = request.form.get("userid")
    user = User.query.filter(User.userid == userid).first()
    if user.adminright:
        user.adminright = False
    else:
        user.adminright = True
    db.session.commit()
    return redirect(url_for('admin_bp.users'))


@admin_bp.route('/changeBlockade', methods=['POST'])
def change_blockade():
    userid = request.form.get("userid")
    user = User.query.filter(User.userid == userid).first()
    if user.banned:
        user.banned = False
    else:
        user.banned = True
    db.session.commit()
    return redirect(url_for('admin_bp.users'))


@admin_bp.route('/changeBlockade_store', methods=['POST'])
def change_blockade_store():
    storeid = request.form.get("storeid")
    store = Store.query.filter(Store.storeid == storeid).first()
    if store.blockade:
        store.blockade = False
    else:
        store.blockade = True
    db.session.commit()
    return redirect(url_for('admin_bp.stores'))


@admin_bp.route('/addComment', methods=['POST'])
def add_comment():
    storeid = request.form.get("storeid")
    store = Store.query.filter(Store.storeid == storeid).first()
    comment = commentstore.query\
        .filter(commentstore.storeid == storeid).filter(commentstore.adminid == current_user.userid).first()
    if request.form.get("empty"):
        return render_template('comment.html', title='Komentar', store=store, comment=comment)
    else:
        comment_content = request.form.get("comment")
        new_comment = commentstore(
            adminid=current_user.userid,
            storeid=storeid,
            comcontent=comment_content
        )
        db.session.add(new_comment)
        db.session.commit()
        comment = commentstore.query\
            .filter(commentstore.storeid == storeid).filter(commentstore.adminid == current_user.userid).first()
        return render_template('comment.html', title='Komentra', store=store, comment=comment, added=True)


@admin_bp.route('/updateComment', methods=['POST'])
def update_comment():
    comment_content = request.form.get("comment")
    storeid = request.form.get("storeid")
    store = Store.query.filter(Store.storeid == storeid).first()
    comment = commentstore.query\
        .filter(commentstore.storeid == storeid).filter(commentstore.adminid == current_user.userid).first()
    comment.comcontent = comment_content
    db.session.commit()
    return render_template('comment.html', title='Komentar', store=store, comment=comment, changed=True)