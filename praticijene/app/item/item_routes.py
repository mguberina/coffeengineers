from flask import Blueprint, render_template, request
from flask import current_app as app
from app.models import User, iteminstore, Item, Store, Tag, ItemTag, UserTag, Pricechange
from flask_login import login_required, current_user
import re
from app import db

item_bp = Blueprint('item_bp', __name__,
                    template_folder='templates')


@item_bp.route('/proizvod_podaci', methods=['GET', 'POST'])
def proizvodPodaci():
    item = Item.query.filter(Item.gtin == request.form.get("gtin")).first()
    stores = iteminstore.query.filter(iteminstore.itemid == request.form.get("gtin")).all()
    tags = db.session.query(ItemTag, Tag) \
        .filter(ItemTag.tagid == Tag.tagid) \
        .filter(ItemTag.itemid == request.form.get("gtin")) \
        .order_by(ItemTag.tagacc.desc()).limit(5).all()
    return render_template('proizvodPodaci.html', title='Proizvod', item=item, stores=stores, tags=tags)


@item_bp.route("/priceChanges", methods=["GET", "POST"])
def price_changes():
    if request.method == "POST":
        item_id = request.form.get("gtin")
        store_id = request.form.get("storeid")
        item_name = Item.query.filter(Item.gtin == item_id).first().itemname
        changes = Pricechange.query.filter(Pricechange.storeid == store_id).filter(Pricechange.item == item_id).all()
        print(changes)
        return render_template("priceChanges.html", title="Promjene cijena", itemname=item_name, changes=changes,
                               gtin=item_id, storeid=store_id)
    return render_template("priceChanges.html", title="Promjene cijena")
