# coding: utf-8
from sqlalchemy import Boolean, CheckConstraint, Column, Date, Float, ForeignKey, Integer, String, Table, Time, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import UserMixin
from . import db

# initing the database

# db.create_all()

Base = declarative_base()
metadata = Base.metadata

class User(UserMixin, db.Model):
    __tablename__ = 'users'

    userid = Column(Integer, primary_key=True)
    firstname = Column(String(64), nullable=False)
    lastname = Column(String(64), nullable=False)
    username = Column(String(64), unique=True)
    email = Column(String(64), nullable=False, unique=True)
    adminright = Column(Boolean, nullable=False)
    nameprivate = Column(Boolean, server_default=text("false"))
    emailprivate = Column(Boolean, server_default=text("false"))
    banned = Column(Boolean, server_default=text("false"))
    passwordhash = Column(String(128))
    isstore = Column(Boolean, server_default=text("false"))

    def get_id(self):
        return self.userid

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Store(UserMixin, db.Model):
    __tablename__ = 'store'
    __table_args__ = (
        CheckConstraint('weekendstart < weekendend'),
        CheckConstraint('workingdaystart < workingdayend')
    )
    storeid = Column(Integer, primary_key=True)
    username = Column(String(64), nullable=False)
    city = Column(String(64), nullable=False)
    address = Column(String(64), nullable=False)
    workingdaystart = Column(Time, nullable=False)
    workingdayend = Column(Time, nullable=False)
    weekendstart = Column(Time)
    weekendend = Column(Time)
    blockade = Column(Boolean, server_default=text("false"))
    passwordhash = Column(String(32))
    isstore = Column(Boolean, server_default=text("false"))
    itemLocation = db.relationship("iteminstore", back_populates="storename")
    #adminchange = db.relationship('Adminchange', secondary='banstore')

    def get_id(self):
        return self.storeid

    def __repr__(self):
        return '<User {}>'.format(self.username)



class Item(db.Model):
    __tablename__ = 'item'
    gtin = Column(Integer, primary_key=True)
    itemname = Column(String(64), nullable=False)
    inStore = db.relationship("iteminstore", back_populates="item")


class iteminstore(db.Model):
    __tablename__ = 'iteminstore'
    itemid = Column(Integer, ForeignKey('item.gtin'), primary_key=True)
    item = db.relationship("Item", back_populates="inStore")
    storename = db.relationship("Store", back_populates="itemLocation")
    storeid = Column(Integer, ForeignKey('store.storeid'), primary_key=True)
    amount = Column(Integer)
    price = Column(Float(53))

"""models.py
class Adminchangetype(db.Model):
    __tablename__ = 'adminchangetype'

    typeid = Column(Integer, primary_key=True)
    typename = Column(String(16), nullable=False)

"""
"""

class Picture(db.Model):
    __tablename__ = 'picture'

    pictureid = Column(Integer, primary_key=True)
    picturepath = Column(String(128), nullable=False, unique=True)

"""

"""
class Tag(db.Model):
    __tablename__ = 'tag'

    tagid = Column(Integer, primary_key=True)
    tagname = Column(String(64), nullable=False)

"""

"""
created_on = db.Column(db.DateTime,
                           index=False,
                           unique=False,
                           nullable=True)
    last_login = db.Column(db.DateTime,
                           index=False,
                           unique=False,
                           nullable=True)
"""

#    adminchange = relationship('Adminchange', secondary='banuser')

"""
class Adminchange(db.Model):
    __tablename__ = 'adminchange'

    changeid = Column(Integer, primary_key=True)
    changedby = Column(ForeignKey('users.username'), nullable=False)
    changetypeid = Column(ForeignKey('adminchangetype.typeid'), nullable=False)
    changedate = Column(Date, nullable=False)

    auth = relationship('User')
    adminchangetype = relationship('Adminchangetype')

"""

"""


class itemtag(db.Model): 
    __tablename__ = 'itemtag'
    itemid = Column(Integer, ForeignKey('item.gtin'), primary_key=True)
    tagid = Column(Integer, ForeignKey('tag.tagid'), primary_key=True)
    tagacc = Column(Integer)



class Pricechange(db.Model):
    __tablename__ = 'pricechange'

    changeid = Column(Integer, primary_key=True)
    proposedby = Column(ForeignKey('users.username'), nullable=False)
    item = Column(ForeignKey('item.gtin'), nullable=False)
    storeid = Column(ForeignKey('store.storeid'), nullable=False)
    changedate = Column(Date, nullable=False)
    pictureid = Column(String(128), nullable=False)
    reviewedby = Column(ForeignKey('users.username'), nullable=False)
    approved = Column(Boolean)

    item1 = relationship('Item')
    auth = relationship('User', primaryjoin='Pricechange.proposedby == User.username')
    user1 = relationship('User', primaryjoin='Pricechange.reviewedby == User.username')
    store = relationship('Store')


class banstore(db.Model): 
    __tablename__ = 'banstore'
    banid = Column(Integer, ForeignKey('Adminchange.changeid'), primary_key=True)
    banid_r = db.relationship('Adminchange', primaryjoin= 'Adminchange.changeid == banstore.banid')
    banedstore = Column(Integer, ForeignKey('store.storeid'), primary_key=True)
    banedstore_r = db.relationship('Store', primaryjoin='Store.storeid == banstore.banedstore')


class banuser(db.Model):
    __tablename__ = 'banuser'
    banid = Column(Integer, ForeignKey('adminchange.changeid'), primary_key=True, nullable=False)
    baneduser = Column(String(64), ForeignKey('users.username'), primary_key=True, nullable=False)
    banneduser_r = relationship('User', primaryjoin = 'banuser.baneduser == User.username')
    banid_r = relationship('adminchange', primaryjoin = 'banuser.banid == adminchange.changeid')

#class _banuser(db.Model): 
#    __tablename__ = 'banuser'
#    Column('banid', ForeignKey('adminchange.changeid'), primary_key=True, nullable=False),
#    Column('baneduser', ForeignKey('users.username'), primary_key=True, nullable=False))


class commentstore(db.Model): 
    __tablename__ = 'commentstore'
    commentid = Column('commentid', ForeignKey('adminchange.changeid'), primary_key=True)
    storeid = Column('storeid', ForeignKey('store.storeid'),primary_key=True)
    comcontent = Column('comcontent', String(512))
    commentid_r = relationship('commentid', primaryjoin = 'adminchange.changeid == commentstore.commentid')
    storeid_r = relationship('storeid', primaryjoin = 'store.storeid == commentstore.storeid')

"""
