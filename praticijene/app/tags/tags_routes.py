from flask import Blueprint, render_template, request
from flask import current_app as app
from app.models import User, iteminstore, Item, Store, Tag, ItemTag, UserTag
from flask_login import login_required, current_user
import re
from app import db


tags_bp = Blueprint('tags_bp', __name__,
                    template_folder='templates')


@tags_bp.route('/addTag', methods=['GET','POST'])
def addTags():
    if request.method == 'POST':
        itemid = request.form.get("gtin")
        already_added = db.session.query(UserTag, Tag) \
            .filter(UserTag.tagid == Tag.tagid) \
            .filter(UserTag.userid == current_user.userid) \
            .filter(UserTag.itemid == itemid).all()
        size = len(already_added)
        item = Item.query.filter(Item.gtin == itemid).first()
        if request.form.get("empty"):
            return render_template('dodajOznake.html', title='Dodavanje oznaka', item=item, existing=already_added, size=size)
        else:
            oznaka = request.form.get("oznaka")
            if already_added:
                for item in already_added:
                    if item.Tag.tagname == oznaka:
                        sent_item = Item.query.filter(Item.gtin == itemid).first()
                        return render_template('dodajOznake.html', title='Dodavanje oznaka', item=sent_item, existing=already_added, size=size, exists=True)

            tag_exists = False
            allTags = db.session.query(ItemTag, Tag) \
                .filter(ItemTag.tagid == Tag.tagid) \
                .filter(ItemTag.itemid == itemid).all()
            if allTags:
                for item in allTags:
                    if item.Tag.tagname == oznaka:
                        itemtag = ItemTag.query.filter(ItemTag.itemid == itemid).first()
                        itemtag.tagacc = itemtag.tagacc + 1
                        db.session.commit()
                        tag_exists = True
            if not tag_exists:
                tag = Tag.query.filter(Tag.tagname == oznaka).first()
                if not tag:
                    new_tag = Tag(
                        tagname = oznaka
                    )
                    db.session.add(new_tag)
                    db.session.commit()
                    tag = Tag.query.filter(Tag.tagname == oznaka).first()
                new_itemtag = ItemTag(
                    itemid=itemid,
                    tagid=tag.tagid,
                    tagacc=1
                )
                db.session.add(new_itemtag)
                db.session.commit()

            tag = Tag.query.filter(Tag.tagname == oznaka).first()
            new_usertag = UserTag(
                userid=current_user.userid,
                itemid=itemid,
                tagid=tag.tagid
            )
            db.session.add(new_usertag)
            db.session.commit()
            already_added = db.session.query(UserTag, Tag) \
                .filter(UserTag.tagid == Tag.tagid) \
                .filter(UserTag.userid == current_user.userid) \
                .filter(UserTag.itemid == itemid).all()
            size = len(already_added)
            item = Item.query.filter(Item.gtin == itemid).first()
            return render_template('dodajOznake.html', title='Dodavanje oznaka', item=item, existing=already_added, size=size, added=True)

@tags_bp.route('/deleteTag', methods=['POST'])
def deleteTags():
    itemid = request.form.get("itemid")
    tagid = request.form.get("tagid")
    userid = current_user.userid
    UserTag.query\
        .filter(UserTag.userid == userid)\
        .filter(UserTag.tagid == tagid) \
        .filter(UserTag.itemid == itemid).delete()
    db.session.commit()

    itemtag = ItemTag.query.filter(ItemTag.itemid == itemid).filter(ItemTag.tagid == tagid).first()
    if itemtag.tagacc > 1:
        itemtag.tagacc = itemtag.tagacc - 1
        db.session.commit()
    else:
        db.session.delete(itemtag)
        db.session.commit()
    already_added = db.session.query(UserTag, Tag) \
        .filter(UserTag.tagid == Tag.tagid) \
        .filter(UserTag.userid == current_user.userid) \
        .filter(UserTag.itemid == itemid).all()
    size = len(already_added)
    item = Item.query.filter(Item.gtin == itemid).first()
    return render_template('dodajOznake.html', title='Dodavanje oznaka', item=item, existing=already_added, size=size)