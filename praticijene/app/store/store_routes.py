from flask import Blueprint, render_template, request
from flask import current_app as app
from app.models import User, iteminstore, Item, Store, Tag, ItemTag, UserTag, commentstore, Pricechange
from flask_login import login_required, current_user
import re
from app import db
from datetime import date

store_bp = Blueprint('store_bp', __name__,
                    template_folder='templates')

@store_bp.route('/trgovina_podaci', methods=['GET', 'POST'])
def trgovinaPodaci():
    storeid = request.form.get("storeid")
    store = Store.query.filter(Store.storeid == storeid).first()
    items = iteminstore.query.filter(iteminstore.storeid == storeid)
    comments = commentstore.query.filter(commentstore.storeid == storeid).all()
    today = date.today()
    changes = Pricechange.query.filter(Pricechange.storeid == storeid)\
        .filter(Pricechange.admin == True)\
        .filter(Pricechange.changedate == today).all()
    return render_template('trgovinaPodaci.html', title='Trgovina', store=store, items=items, comments=comments, size=len(changes))