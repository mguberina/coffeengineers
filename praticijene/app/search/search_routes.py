from flask import Blueprint, render_template, request
from flask import current_app as app
from app.models import User, iteminstore, Item, Store, Tag, ItemTag, UserTag
from flask_login import login_required, current_user
import re
from app import db
from sqlalchemy import func

search_bp = Blueprint('search_bp', __name__,
                    template_folder='templates')


@search_bp.route('/proizvodi_search', methods=['POST', 'GET'])
def proizvodiSearch():
    if request.method == 'POST':
        if request.form.get("radio") == 'naziv':
            naziv = request.form.get("naziv")
            result = db.session.query(Item, iteminstore)\
                .filter(Item.gtin == iteminstore.itemid)\
                .filter(func.lower(Item.itemname).like('%' + func.lower(naziv) +'%')).distinct(Item.itemname, iteminstore.itemid)\
                .all()
            return render_template('proizvodiSearch.html', title='Pretraživanje proizvoda', items=result)
        elif request.form.get("radio") == 'oznaka':
            oznaka = request.form.get("oznaka")
            result = (db.session.query(Item, ItemTag, Tag)
                      .filter(Item.gtin == ItemTag.itemid)
                      .filter(ItemTag.tagid == Tag.tagid)
                      .filter(Tag.tagname == oznaka)
                      .all())
            return render_template('proizvodiSearch.html', title='Pretraživanje proizvoda', items=result)
        else:
            result = (db.session.query(Item, iteminstore)
                      .filter(Item.gtin == iteminstore.itemid).distinct(Item.itemname, iteminstore.itemid)
                      .all())
            return render_template('proizvodiSearch.html', title='Pretraživanje proizvoda', items=result, radioError=True)
    else:
        result = (db.session.query(Item, iteminstore)
              .filter(Item.gtin == iteminstore.itemid).distinct(Item.itemname, iteminstore.itemid)
              .all())
        return render_template('proizvodiSearch.html', title='Pretraživanje proizvoda', items=result)


@search_bp.route('/trgovine_search', methods=['POST', 'GET'])
def trgovineSearch():
    if request.method == 'POST':
        address = request.form.get("address")
        city = request.form.get("city")
        if request.form.get("addressCheck"):
            if request.form.get("cityCheck"):
                if address and city:
                    stores = Store.query.filter(Store.blockade == False)\
                        .filter(func.lower(Store.address).like('%' + func.lower(address) + '%'))\
                        .filter(func.lower(Store.city).like('%' + func.lower(city) + '%')).all()
                    return render_template('trgovineSearch.html', title='Pretraživanje trgovina', stores=stores)
            else:
                if address:
                    stores = Store.query.filter(Store.blockade == False)\
                        .filter(func.lower(Store.address).like('%' + func.lower(address) + '%')).all()
                    return render_template('trgovineSearch.html', title='Pretraživanje trgovina', stores=stores)
        elif request.form.get("cityCheck"):
            stores = Store.query.filter(Store.blockade == False)\
                        .filter(func.lower(Store.city).like('%' + func.lower(city) + '%')).all()
            return render_template('trgovineSearch.html', title='Pretraživanje trgovina', stores=stores)
        else:
            stores = Store.query.filter(Store.blockade == False).all()
            return render_template('trgovineSearch.html', title='Pretraživanje trgovina', stores=stores, emptyCheck=True)
        stores = Store.query.filter(Store.blockade == False).all()
        return render_template('trgovineSearch.html', title='Pretraživanje trgovina', stores=stores, empty=True)
    stores = Store.query.filter(Store.blockade == False).all()
    return render_template('trgovineSearch.html', title='Pretraživanje trgovina', stores=stores)

