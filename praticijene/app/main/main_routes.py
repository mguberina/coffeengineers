from flask import Blueprint, render_template, request, redirect, url_for
from flask import current_app as app
from app.models import User, iteminstore, Item, Store, Tag, ItemTag, UserTag, Pricechange
from flask_login import login_required, current_user
import re
from app import db

# postavljanje Blueprinta


main_bp = Blueprint('main_bp', __name__,
                    template_folder='templates')


@main_bp.route('/')
@main_bp.route('/index')
def index():
    requests = Pricechange.query.filter(Pricechange.reviewedby == None).filter(Pricechange.admin == True).all()
    size = len(requests)
    return render_template('index.html', title='Home', requests=requests, size=size)


@main_bp.route("/proizvodi", methods=['GET', 'POST'])
def proizvodi():
    result = (db.session.query(Item, iteminstore, Store)
              .filter(Item.gtin == iteminstore.itemid)
              .filter(iteminstore.storeid == Store.storeid)
              .filter(Store.username == current_user.username)
              .all())
    return render_template('proizvodi.html', title='Proizvodi', items=result)



@main_bp.route("/dodajProizvodTrgovini", methods=["GET", "POST"])
def dodajproizvodtrgovini():
    if request.method == "POST":
        itemname_test = re.sub(r'[^a-z0-9 ]', '', request.form.get("itemname").lower())
        if Item.query.filter((Item.itemname == itemname_test)).first() is None:
            # proizvod ne postoji u bazi podataka, treba ga dodati
            item = Item(
                itemname=itemname_test
            )
            db.session.add(item)
            db.session.commit()

        iteminstore_gtin = Item.query.filter((Item.itemname == itemname_test)).first().gtin
        store_id = Store.query.filter(Store.username == current_user.username).first().storeid
        if (iteminstore.query.filter(iteminstore_gtin == iteminstore.itemid)
                .filter(iteminstore.storeid == store_id).first()) is None:
            # ne postoji zapis o tom proizvodu u toj trgovini
            item_amount = request.form.get("amount")
            item_price = request.form.get("price")
            store_id = Store.query.filter((Store.username == current_user.username)).first().storeid
            item_in_store = iteminstore(
                itemid=iteminstore_gtin,
                storeid=store_id,
                price=item_price,
                amount=item_amount
            )

            db.session.add(item_in_store)
            db.session.commit()
            return render_template("dodajProizvodTrgovini.html", title="Dodavanje proizvoda trgovini", exists=False,
                                   added=True)
        else:
            # item već postoji u trgovini, mijenja mu se količina (stara + nova)
            item_amount = int(iteminstore.query.filter((iteminstore_gtin == iteminstore.itemid)).first().amount) \
                          + int(request.form.get("amount"))
            item_in_store = iteminstore.query.filter((iteminstore_gtin == iteminstore.itemid)).first()
            item_in_store.amount = item_amount

            db.session.commit()
            return render_template("dodajProizvodTrgovini.html", title="Dodavanje proizvoda trgovini", exists=True,
                                   added=True)

    return render_template("dodajProizvodTrgovini.html", title="Dodavanje proizvoda trgovini")


@main_bp.route('/obrisiProizvod', methods=['POST'])
def obrisiProizvod():
    itemInStore = iteminstore.query.filter(iteminstore.storeid == request.form.get("storeid"))\
        .filter(iteminstore.itemid == request.form.get("itemid")).first()
    db.session.delete(itemInStore)
    db.session.commit()
    return redirect(url_for('main_bp.proizvodi'))