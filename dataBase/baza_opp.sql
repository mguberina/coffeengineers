DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

CREATE TABLE users (
	firstName 	VARCHAR(64)	NOT NULL,
	lastName	VARCHAR(64)	NOT NULL,
	userName	VARCHAR(64)	NOT NULL PRIMARY KEY,
	eMail	VARCHAR(64)	NOT NULL UNIQUE,
	adminRight	BOOLEAN	NOT NULL,
	namePrivate	BOOLEAN DEFAULT False,
	emailPrivate BOOLEAN DEFAULT False,
	banned BOOLEAN DEFAULT False,
	passwordHash VARCHAR(32)
);

CREATE TABLE Store (
	storeID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	adress VARCHAR(128)	NOT NULL,
	workingDayStart	TIME NOT NULL,
	workingDayEnd TIME NOT NULL,
	weekendStart TIME,
	weekendEnd TIME,
	blocade BOOLEAN DEFAULT False,
	passwordHash VARCHAR(32) NOT NULL,
	wrongPriceCount INTEGER DEFAULT 0,
	CONSTRAINT time_check1 CHECK (workingDayStart < workingDayEnd),
	CONSTRAINT time_check2 CHECK (weekendStart < weekendEnd)
);

CREATE TABLE StoreName (
    storeID INTEGER REFERENCES Store(storeID),
    storeName VARCHAR(32) NOT NULL,
	PRIMARY KEY (storeID, storeName)
);

CREATE TABLE AdminChangeType (
	typeID		INTEGER	GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	typeName	VARCHAR(16)	NOT NULL
);

CREATE TABLE AdminChange (
	changeID	INTEGER	GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	changedBy	VARCHAR(64) NOT NULL REFERENCES users(userName),
	changeTypeID	INTEGER NOT NULL REFERENCES AdminChangeType(typeID),
	changeDate	DATE NOT NULL	
);

CREATE TABLE BanUser (
	banID INTEGER REFERENCES AdminChange(changeID),
	banedUser VARCHAR(64) REFERENCES users(userName),
	PRIMARY KEY (banID, banedUser)
);

CREATE TABLE BanStore (
	banID INTEGER NOT NULL REFERENCES AdminChange(changeID),
	banedStore INTEGER NOT NULL REFERENCES Store(storeID),
	PRIMARY KEY (banID, banedStore)
);

CREATE TABLE CommentStore (
	commentID INTEGER NOT NULL REFERENCES AdminChange(changeID),
	storeID	INTEGER NOT NULL REFERENCES Store(storeID),
	comContent	VARCHAR(512),
	PRIMARY KEY (commentID, storeID)
);

CREATE TABLE Item (
	gtin	INTEGER	GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	itemName	VARCHAR(64) NOT NULL
);

CREATE TABLE PriceChange (
	changeID	INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	proposedBy	VARCHAR(64) NOT NULL REFERENCES users(userName),
	item	INTEGER NOT NULL REFERENCES item(gtin),
	storeID	INTEGER NOT NULL REFERENCES store(storeID),
	changeDate 	DATE NOT NULL,
	pictureID	VARCHAR (128) NOT NULL,
	reviewedBy VARCHAR(64) NOT NULL REFERENCES users(userName),
	approved BOOLEAN DEFAULT NULL
);

CREATE TABLE ItemInStore (
	itemID INTEGER REFERENCES item(gtin),
	storeID INTEGER REFERENCES store(storeID),
	price	FLOAT,
	PRIMARY KEY (itemID, storeID)
);

CREATE TABLE Tag (
	tagID	INTEGER	GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	tagName VARCHAR(64)	NOT NULL
);

CREATE TABLE ItemTag (
	itemID INTEGER REFERENCES item(gtin),
	tagID INTEGER REFERENCES tag(tagID),
	tagAcc	INTEGER,
	PRIMARY KEY (itemID, tagID)
);

CREATE TABLE Picture (
	pictureID INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	picturePath VARCHAR(128) NOT NULL UNIQUE
);

INSERT INTO AdminChangeType(typeName) VALUES('UserBan');
INSERT INTO AdminChangeType(typeName) VALUES('StoreBan');
INSERT INTO AdminChangeType(typeName) VALUES('StoreComment');
--insert into users('jedan', 'dva', 'tri', 'cetiri', 'pet', False);

 
