import bcrypt
from flask import Blueprint, make_response
from flask import current_app as app
from flask import render_template, flash, redirect, request, url_for
from sqlalchemy import and_, func

from app.models import db
from app.models import User, Store

from flask_login import login_required, logout_user, current_user, login_user
from app import login_manager

# postavljanje Blueprinta
auth_bp = Blueprint('user_bp', __name__,
                    template_folder='templates')


@auth_bp.route('/logout')
@login_required
def logout_page():
    """User log-out logic."""
    logout_user()
    return redirect(url_for('user_bp.login'))


@login_manager.user_loader
def load_user(user_id):
    """Check if user is logged-in on every page load."""
    if user_id is not None:
        return User.query.get(user_id)
    return None



@login_manager.unauthorized_handler
def unauthorized():
    """Redirect unauthorized users to Login page."""
    flash('You must be logged in to view that page.')
    return redirect(url_for('user_bp.login'))


@auth_bp.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get("username")
        password = request.form.get("password").encode("utf-8")
        username_check = User.query.filter((func.lower(User.username) == func.lower(username))).first()
        if username_check:
            if bcrypt.checkpw(password, username_check.passwordhash.encode("utf-8")):
                if not username_check.banned:
                    login_user(username_check)
                    return render_template('index.html', loggedIn=True)
                else:
                    return render_template('login.html', title='Prijava', banned=True)
            else:
                return render_template('login.html', title='Prijava', notexistsuser=True)
        else:
            return render_template('login.html', title='Prijava', notexistsuser=True)
    return render_template('login.html', title='Prijava')


@auth_bp.route("/store_login", methods=["GET", "POST"])
def store_login():
    if request.method == 'POST':
        store_id_check = Store.query.filter((func.lower(Store.username) == func.lower(request.form.get("store_username")))).first()
        password = request.form.get("store_password").encode("utf-8")
        if store_id_check:
            user = User.query.filter((func.lower(User.username) == func.lower(request.form.get("store_username")))).first()
            if bcrypt.checkpw(password, store_id_check.passwordhash.encode("utf-8")):
                login_user(user)
                return render_template('index.html', loggedIn=True)
            else:
                return render_template('login.html', title='Prijava', notexists=True)
        else:
            return render_template('login.html', title='Prijava', notexists=True)
    return render_template('login.html', title='Prijava')


@auth_bp.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        errors = validate_user(request)
        if errors:
            return render_template('register2.html', title='Registracija', errors=errors)
        else:
            salt = bcrypt.gensalt()
            hashpw = bcrypt.hashpw(request.form.get("password").encode("utf-8"), salt)
            user = User(
                firstname=request.form.get("firstname"),
                lastname=request.form.get("lastname"),
                username=request.form.get("username"),
                email=request.form.get("email"),
                passwordhash=hashpw.decode("utf-8"),
                adminright=False,
                isstore=False)
            db.session.add(user)
            db.session.commit()
            login_user(user)
            return render_template('index.html', loggedIn=True)
    return render_template('register2.html', title='Registracija')


@auth_bp.route('/store_register', methods=['GET', 'POST'])
def store_register():
    if request.method == 'POST':
        errors = validate_store(request)
        if errors:
            print(errors)
            return render_template('register2.html', title='Registracija', store_errors=errors)
        else:
            salt = bcrypt.gensalt()
            hashpw = bcrypt.hashpw(request.form.get("store_password").encode("utf-8"), salt)
            store = Store(
                username=request.form.get("store_username"),
                storename=request.form.get("store_storename"),
                city=request.form.get("store_city"),
                address=request.form.get("store_address"),
                workingdaystart=request.form.get("workingDayStart"),
                workingdayend=request.form.get("workingDayEnd"),
                weekendstart=request.form.get("weekendStart"),
                weekendend=request.form.get("weekendEnd"),
                passwordhash=hashpw.decode("utf-8"),
                isstore=True
            )
            user = User(
                firstname=request.form.get("store_address"),
                lastname='',
                username=request.form.get("store_username"),
                email=(request.form.get("store_username") + "@store.hr"),
                passwordhash=hashpw.decode("utf-8"),
                adminright=False,
                isstore=True
            )
            db.session.add(user)
            db.session.add(store)
            db.session.commit()
            login_user(user)
            return render_template('index.html', loggedIn=True)
    return render_template('register2.html', title='Registracija')


def validate_store(request):
    errors = []
    username = request.form.get("store_username")
    password = request.form.get("store_password")
    rpassword = request.form.get("store_rpassword")

    username_check = User.query.filter(func.lower(User.username) == func.lower(username)).first()
    if username_check:
        errors.append("Korisničko ime već postoji!")
    if password != rpassword:
        errors.append("Lozinka i ponovljena lozinka moraju biti jednake!")

    return errors


def validate_user(request):
    errors = []
    username = request.form.get("username")
    email = request.form.get("email")
    password = request.form.get("password")
    rpassword = request.form.get("rpassword")

    username_check = User.query.filter(func.lower(User.username) == func.lower(username)).first()
    email_check = User.query.filter(User.email == email).first()
    if username_check:
        errors.append("Korisničko ime već postoji!")
    if email_check:
        errors.append("E-mail je već iskorišten!")
    if password != rpassword:
        errors.append("Lozinka i ponovljena lozinka moraju biti jednake!")
    return errors




