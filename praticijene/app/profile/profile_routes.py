import bcrypt
from flask import Blueprint, render_template, redirect, request, url_for
from flask import current_app as app
from flask_login import login_required, logout_user, current_user, login_user

from app.models import User, db

#postavljanje Blueprinta
profile_bp = Blueprint('profile_bp', __name__,
                    template_folder='templates')

@profile_bp.route('/profile')
def profile():
    user = User.query.filter(User.username == current_user.username).first()
    return render_template('profile.html', title='Profil', user=user)

@profile_bp.route('/update_profile', methods=['POST'])
def update():
    user = User.query.filter(User.username == current_user.username).first()
    if request.form.get("nameprivate"):
        user.nameprivate = False
    else:
        user.nameprivate = True
    if request.form.get("emailprivate"):
        user.emailprivate = False
    else:
        user.emailprivate = True
    db.session.commit()
    return render_template('profile.html', title='Profil', user=user)


@profile_bp.route('/update_password', methods=['POST'])
def update_pass():
    password = request.form.get("newpassword")
    rpassword = request.form.get("rnewpassword")
    print(password)
    if password != rpassword:
        return render_template('profile.html', title='Profil', wrong=True)
    else:
        user = User.query.filter(User.username == current_user.username).first()
        salt = bcrypt.gensalt()
        hashpw = bcrypt.hashpw(password.encode("utf-8"), salt)
        user.passwordhash = hashpw.decode("utf-8")
        db.session.commit()
        return render_template('index.html', title='Profil', changed=True)


@profile_bp.route('/delete_acc')
def delete():
    user = User.query.filter(User.username == current_user.username).first()
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('main_bp.index'))
