# coding: utf-8
from sqlalchemy import Boolean, CheckConstraint, Column, Date, Float, ForeignKey, Integer, String, Table, Time, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import UserMixin
from . import db

# initing the database

# db.create_all()

Base = declarative_base()
metadata = Base.metadata

class User(UserMixin, db.Model):
    __tablename__ = 'users'

    userid = Column(Integer, primary_key=True)
    firstname = Column(String(64), nullable=False)
    lastname = Column(String(64), nullable=False)
    username = Column(String(64), unique=True)
    email = Column(String(64), nullable=False, unique=True)
    adminright = Column(Boolean, nullable=False)
    nameprivate = Column(Boolean, server_default=text("false"))
    emailprivate = Column(Boolean, server_default=text("false"))
    banned = Column(Boolean, server_default=text("false"))
    passwordhash = Column(String(128))
    isstore = Column(Boolean, server_default=text("false"))

    def get_id(self):
        return self.userid

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Store(UserMixin, db.Model):
    __tablename__ = 'store'
    __table_args__ = (
        CheckConstraint('weekendstart < weekendend'),
        CheckConstraint('workingdaystart < workingdayend')
    )
    storeid = Column(Integer, primary_key=True)
    username = Column(String(64), nullable=False)
    storename = Column(String(64), nullable=False)
    city = Column(String(64), nullable=False)
    address = Column(String(64), nullable=False)
    workingdaystart = Column(Time, nullable=False)
    workingdayend = Column(Time, nullable=False)
    weekendstart = Column(Time)
    weekendend = Column(Time)
    blockade = Column(Boolean, server_default=text("false"))
    passwordhash = Column(String(32))
    itemLocation = db.relationship("iteminstore", back_populates="storename")
    isstore = Column(Boolean, server_default=text("true"))

    #adminchange = db.relationship('Adminchange', secondary='banstore')

    def get_id(self):
        return self.storeid

    def __repr__(self):
        return '<User {}>'.format(self.username)



class Item(db.Model):
    __tablename__ = 'item'
    gtin = Column(Integer, primary_key=True)
    itemname = Column(String(64), nullable=False)
    #inStoreID = Column(db.Integer, db.ForeignKey("iteminstore.itemid"), nullable=False)
    inStore = db.relationship("iteminstore", back_populates="item")
    #storeid = Column(Integer, ForeignKey('store.itemLocation'), primary_key=True)


class iteminstore(db.Model):
    __tablename__ = 'iteminstore'
    itemid = Column(db.Integer, db.ForeignKey('item.gtin'), primary_key=True)
    item = db.relationship("Item", back_populates="inStore")
    storename = db.relationship("Store", back_populates="itemLocation")
    storeid = Column(db.Integer, db.ForeignKey('store.storeid'), primary_key=True)
    amount = Column(Integer)
    price = Column(Float(53))


class Pricechange(db.Model):
    __tablename__ = 'pricechange'

    changeid = Column(Integer, primary_key=True)
    proposedby = Column(ForeignKey('users.username'), nullable=False)
    item = Column(ForeignKey('item.gtin'), nullable=False)
    storeid = Column(ForeignKey('store.storeid'), nullable=False)
    changedate = Column(Date)
    pictureid = Column(String(128))
    reviewedby = Column(ForeignKey('users.username'))
    approved = Column(Boolean)
    admin = Column(Boolean)
    oldprice = Column(Float)
    newprice = Column(Float)

    item1 = relationship('Item')
    auth = relationship('User', primaryjoin='Pricechange.proposedby == User.username')
    user1 = relationship('User', primaryjoin='Pricechange.reviewedby == User.username')
    store = relationship('Store')


class Picture(db.Model):
    __tablename__ = 'picture'

    pictureid = Column(Integer, primary_key=True)
    picturepath = Column(String(128), nullable=False, unique=True)


class Tag(db.Model):
    __tablename__ = 'tag'

    tagid = Column(Integer, primary_key=True)
    tagname = Column(String(64), nullable=False)


class ItemTag(db.Model):
    __tablename__ = 'itemtag'
    itemid = Column(Integer, ForeignKey('item.gtin'), primary_key=True)
    tagid = Column(Integer, ForeignKey('tag.tagid'), primary_key=True)
    tagacc = Column(Integer)


class UserTag(db.Model):
    __tablename__ = "usertag"

    itemid = Column(db.Integer, primary_key=True)
    tagid = Column(db.Integer, ForeignKey('tag.tagid'))
    userid = Column(db.Integer, primary_key=True)


class banuser(db.Model):
    __tablename__ = 'banuser'

    banid = Column(Integer, primary_key=True)
    baneduser = Column(Integer, nullable=False)
    adminid = Column(Integer, nullable=False)


class banstore(db.Model):
    __tablename__ = 'banstore'

    banid = Column(db.Integer, primary_key=True)
    banedstore = Column(db.Integer, nullable=False)
    adminid = Column(db.Integer, nullable=False)


class commentstore(db.Model):
    __tablename__ = 'commentstore'

    commentid = Column(db.Integer, primary_key=True)
    storeid = Column(db.Integer)
    comcontent = Column(String(512))
    adminid = Column(db.Integer, nullable=False)
