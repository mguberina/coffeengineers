import os

from flask import Blueprint, render_template, request, redirect, url_for
from flask import current_app as app
from app.models import User, iteminstore, Item, Store, Tag, ItemTag, UserTag, Pricechange, Picture
from flask_login import login_required, current_user
import re
from app import db
from datetime import date

# postavljanje Blueprinta


price_change_request_bp = Blueprint('price_change_request_bp', __name__,
                                    template_folder='templates')

app.config["IMAGE_UPLOADS"] = "app/static/img"


@price_change_request_bp.route("/priceChangeRequest", methods=["GET", "POST"])
def price_change_request():
    gtin = request.form.get("gtin")
    store_id = request.form.get("storeid")
    print(gtin)
    print(store_id)

    store = Store.query.filter(store_id == Store.storeid).first()
    item = Item.query.filter(gtin == Item.gtin).first()
    item_in_store = iteminstore.query.filter(gtin == iteminstore.itemid) \
        .filter(store_id == iteminstore.storeid).first()
    if request.method == "POST":
        if request.files:
            image = request.files["image"]
            print(image)
            image.save(os.path.join(app.config["IMAGE_UPLOADS"], image.filename))
            pic = Picture(
                picturepath=image.filename
            )
            db.session.add(pic)
            db.session.commit()
            price_change = Pricechange(
                proposedby=current_user.userid,
                item=request.form.get("gtin"),
                storeid=request.form.get("storeid"),
                pictureid=(pic.query.filter(image.filename == Picture.picturepath)).first().pictureid,
		newprice=request.form.get("rightprice"),
                approved=False,
                admin=True
            )
            db.session.add(price_change)
            db.session.commit()

            return render_template("priceChangeRequest.html", title="Predaja zahtjeva za promjenu cijene",
                                   store=store, item=item, iteminstore=item_in_store, added=True)

    return render_template("priceChangeRequest.html", title="Predaja zahtjeva za promjenu cijene",
                           store=store, item=item, iteminstore=item_in_store)


@price_change_request_bp.route('/requests', methods=['GET'])
def requests():
    requests = db.session.query(Pricechange, Picture, Item, Store, iteminstore).\
        filter(Pricechange.reviewedby == None)\
        .filter(Pricechange.storeid == Store.storeid) \
        .filter(Pricechange.item == Item.gtin) \
        .filter(iteminstore.itemid == Pricechange.item) \
        .filter(iteminstore.storeid == Pricechange.storeid) \
        .distinct(Pricechange.changeid) \
        .all()
    return render_template('requests.html', title='Zahtjevi za promjenom cijene', requests=requests)


@price_change_request_bp.route('/zahtjevPodaci', methods=['GET', 'POST'])
def zahtjevPodaci():
    one_request = db.session.query(Pricechange, Picture, Item, Store, iteminstore). \
        filter(Pricechange.reviewedby == None) \
        .filter(Pricechange.storeid == Store.storeid) \
        .filter(Pricechange.item == Item.gtin) \
        .filter(iteminstore.itemid == Pricechange.item) \
        .filter(iteminstore.storeid == Pricechange.storeid) \
        .filter(Pricechange.changeid == request.form.get("changeid")) \
	.filter(Pricechange.pictureid == Picture.pictureid) \
        .first()
    return render_template('zahtjevPodaci.html', title='Pregled zahtjeva', request=one_request)


@price_change_request_bp.route('/odobriPromjenu', methods=['POST'])
def odobriPromjenu():
    itemid = request.form.get("itemid")
    storeid = request.form.get("storeid")
    newprice = request.form.get("newprice")
    itemInStore = iteminstore.query.filter(iteminstore.storeid == storeid)\
        .filter(iteminstore.itemid == itemid).first()
    itemInStore.price = newprice

    adminid = current_user.userid
    today = date.today()
    changeRequest = Pricechange.query.filter(Pricechange.changeid == request.form.get("changeid")).first()
    changeRequest.reviewedby = adminid
    changeRequest.admin = True
    changeRequest.changedate = today
    changeRequest.approved = True
    db.session.commit()
    return redirect(url_for('price_change_request_bp.requests'))


@price_change_request_bp.route('/odbaciPromjenu', methods=['POST'])
def odbaciPromjenu():
    adminid = current_user.userid
    changeRequest = Pricechange.query.filter(Pricechange.changeid == request.form.get("changeid")).first()
    changeRequest.admin = True
    changeRequest.reviewedby = adminid
    db.session.commit()
    return redirect(url_for('price_change_request_bp.requests'))
