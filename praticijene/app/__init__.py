from flask import Flask
from config import Config
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap
from flask_fontawesome import FontAwesome

login_manager = LoginManager()
db = SQLAlchemy()



def create_app():
    # __name__ is a predefined variable in the Flask class
    # it sets the name of the module in which it is used
    app = Flask(__name__, instance_relative_config=False)

    # let's import our configuration (in a different class 'cos decomposition is good)
    app.config.from_object(Config)


    migrate = Migrate(app, db)
    db.init_app(app)
    Bootstrap(app)
    fa = FontAwesome(app)

    login_manager.init_app(app)
    # the bottom import is here to avoid circular imports
    # (because apparently that's a problem with Flask apps

    # routes are different URLs that the application implements
    #from app import routes

    # these routes are !!!to be implemented!!! python functions
    # (handlers) called view functions which are executed on specified URLs

    # registriranje Blueprinta
    with app.app_context():
        from .main import main_routes
        from .auth import auth_routes
        from .profile import profile_routes
        from .search import search_routes
        from .item import item_routes
        from .tags import tags_routes
        from .admin import admin_routes
        from .price_change_request import price_change_request_routes
        from .store import store_routes

        from .file import file_routes
        db.create_all()

        app.register_blueprint(main_routes.main_bp)
        app.register_blueprint(auth_routes.auth_bp)
        app.register_blueprint(profile_routes.profile_bp)
        app.register_blueprint(search_routes.search_bp)
        app.register_blueprint(item_routes.item_bp)
        app.register_blueprint(tags_routes.tags_bp)
        app.register_blueprint(admin_routes.admin_bp)
        app.register_blueprint(store_routes.store_bp)
        app.register_blueprint(price_change_request_routes.price_change_request_bp)
        app.register_blueprint(file_routes.file_bp)
        return app



