import os
from flask import Blueprint, render_template, request
from flask import current_app as app
from app.models import User, iteminstore, Item, Store, Tag, ItemTag, UserTag, Pricechange
from flask_login import login_required, current_user
import re
from app import db
from datetime import date

file_bp = Blueprint('file_bp', __name__,
                    template_folder='templates')

app.config["FILE_UPLOADS"] = "/home/ivan/Documents/git/coffeengineers/praticijene/app/static/file"


@file_bp.route('/uploadFile', methods=["GET", "POST"])
def upload_file():
    if request.method == "POST":
        if request.files:
            store_username = current_user.username
            store_id = Store.query.filter(store_username == Store.username).first().storeid
            print(store_id)
            file = request.files["file"]
            file.save(os.path.join(app.config["FILE_UPLOADS"], "update_file"))
            with open('/home/ivan/Documents/git/coffeengineers/praticijene/app/static/file/update_file') as file_read:
                for line in file_read:
                    parts = line.split(";")
                    if len(parts) == 2:
                        item_name = re.sub(r'[^a-z0-9 ]', '', parts[0].strip().lower())
                        if (Item.query.filter(item_name == Item.itemname).first()) is None:
                            # ne postoji u bazi, dodajemo ga u bazu
                            item = Item(
                                itemname=item_name
                            )
                            db.session.add(item)
                            db.session.commit()
                        item_id = Item.query.filter(item_name == Item.itemname).first().gtin
                        new_price = float(parts[1].strip())
                        old_price = 0.0
                        if (iteminstore.query.filter(item_id == iteminstore.itemid)
                                .filter(store_id == iteminstore.storeid).first()) is None:
                            # ako item nije u trgovini a nalazi se u fileu, item ce se dodat u trgovinu, kolicina 1
                            item_in_store = iteminstore(
                                itemid=item_id,
                                storeid=store_id,
                                price=new_price,
                                amount=1
                            )
                            db.session.add(item_in_store)
                            db.session.commit()
                        else:
                            # postoji u trgovini, treba mu promijeniti cijenu i dodat ga u tablicu price change
                            old_price = iteminstore.query.filter(item_id == iteminstore.itemid) \
                                .filter(store_id == iteminstore.storeid).first().price
                            item_in_store = iteminstore.query.filter((item_id == iteminstore.itemid)).first()
                            item_in_store.price = new_price
                            db.session.commit()
                            # stavljena nova cijena, sada dodati u zapisnik
                            today = date.today()
                            price_change = Pricechange(
                                item=item_id,
                                storeid=store_id,
                                changedate=today,
                                approved=True,
                                admin=False,
                                oldprice=old_price,
                                newprice=new_price
                            )
                            db.session.add(price_change)
                            db.session.commit()
        return render_template("uploadFile.html", title="Slanje datoteke", added=True)
    return render_template("uploadFile.html", title="Slanje datoteke")
