# Generated by Selenium IDE
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class TestTestUC9():
  def setup_method(self, method):
    self.driver = webdriver.Chrome()
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_testUC9(self):
    self.driver.get("https://46.101.168.243/")
    self.driver.set_window_size(1900, 1020)
    self.driver.find_element(By.LINK_TEXT, "Profile").click()
    self.driver.find_element(By.CSS_SELECTOR, ".form-group:nth-child(3) .custom-control-label").click()
    self.driver.find_element(By.CSS_SELECTOR, ".form-group:nth-child(4) .custom-control-label").click()
    self.driver.find_element(By.CSS_SELECTOR, ".form-group-row > .btn").click()
  
