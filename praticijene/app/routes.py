import bcrypt
from flask import render_template, flash, redirect, request, url_for
from sqlalchemy import and_

from app.forms import LoginForm
from app.forms import RegisterForm
from app import app, db
from app.models import User


@app.route('/users')
def users():
    table_users = User.query.all()
    return render_template('users.html', title='Users', users=table_users)
